#!/usr/bin/env python3

from __future__ import print_function
import argparse
import inotify.adapters
import inotify.calls
import os
from send import udp_send

class watch(object):

    """
    **************************************************************************
                                                                            
                               Directory watcher               
                                                                            
    **************************************************************************

    :param this_dir:         Directory to watch (type=str)
    :param enable_send:      Flag to call the sender 
                             (for debugging) (type=bool)
     
    **Description:**                                                           
                                                
    This class uses the inotify python library to set up a watch on a directory.
    If repeatedly looks in this directory to new files and waits until they 
    have finished being written. When they are written it will call a UDP sender
    and tranfer the files elsewhere. 
    ************************************************************************ 
    Author: Benjamin Shaw                                                  

    Email : benjamin.shaw@manchester.ac.uk 

    Web   : http://www.benjaminshaw.me.uk                                  

    ************************************************************************

    **Running the code:**                                                      

    The constructor of the module takes in directory and a flag to set whether
    or not we send the file or just alert (via the command line) whether a new 
    file has been written.

    ``python watch.py -d <directory> -s``

    *************************************************************************
    """

    def __init__(self, this_dir, enable_send=False):

        # Update class variables
        self.this_dir       = this_dir        # Directory to monitor
        self.enable_send    = enable_send     # Flag to send or not send

    def check_dir(self, dirname):
        '''
        Checks directory we're watching exists and
        raises an exception if not

        :param dirname: Path to the directory
                        we are going to watch.
                        This can be relative or
                        absolute (type=str)

        :return:        True if directory exists
                        else False
        '''
        outcome = True
        if not os.path.isdir(dirname):
            outcome = False
        return outcome

    def init_watch(self):
        """
        Creates watch object and adds directory

        :Param None:

        :Return:    Watch object
        """

        ino = inotify.adapters.Inotify()
        try:
            ino.add_watch(self.this_dir)
            print("LISTENING IN: {}".format(self.this_dir))
            return ino
        except inotify.calls.InotifyError as exc:
            raise OSError("Failed to watch {}: {}".format(self.this_dir, exc))
            return None

    def watch_and_send(self, monitor):
        """
        Watches directory. If file is seen it waits
        for writing to close and then sends

        :Param monitor: Send object

        :Return:        None
        """

        for event in monitor.event_gen():

            # Check that event is file and writing is complete
            if event is not None and event[1] == ['IN_CLOSE_WRITE']:

                # Get filename
                this_file = event[3]
                print("{} written".format(this_file))

                relpath = str(event[2]) + "/" + str(event[3]) 
                
                # Do things with file
                if self.enable_send:
                    print("Sending {}".format(relpath)) 
                    sender = udp_send(relpath)
                    sender.send()
                else:
                    print("Found {}. Not sending".format(relpath))

    def run(self):

        # Check directory we're watching actually exists
        if self.check_dir(self.this_dir):

            # Set up watcher
            self.monitor = self.init_watch()

            # Do the watching and act if a file is seen
            self.watch_and_send(self.monitor)

        else:
            # Complain if directory is not found
            raise OSError("No such directory as {}".format(self.this_dir))

def main():
    
    parser = argparse.ArgumentParser(description='Monitors directory for new files')
    parser.add_argument('-d','--dir', help='Directory to watch', required=True)
    parser.add_argument('-s','--enable_send', help='Send file over UDP', required=False, action='store_true')
    args = parser.parse_args()

    watcher = watch(args.dir, args.enable_send)
    watcher.run()

if __name__ == '__main__':
    main()
