import os
import pickle
import pytest
import numpy as np
import sys
import send

DATA_DIR = os.path.dirname(__file__) + "/fixtures"

testfile = DATA_DIR + "/test.spccl"

def get_send_obj():
    testfile = DATA_DIR + "/test.spccl"
    send_obj = send.udp_send(testfile)
    return send_obj

def test_file_checker_true():
    expected=True
    result = get_send_obj().check_file_exists(testfile)
    assert expected == result

def test_file_check_false():
    expected=False
    result = get_send_obj().check_file_exists("nonsense.txt")
    assert expected == result

def test_id_gen():
    expected = 16
    id_str = get_send_obj().id_gen()
    result = len(id_str)
    assert expected == result

def test_get_filesize():
    expected = str(5421)
    result = get_send_obj().get_filesize(testfile)
    assert expected == result

def test_nlines():
    with open(testfile, 'r') as cfile:
        lines = cfile.readlines()
        result = len(lines)
        expected = 34
        assert expected == result
        

